package com.example.interviewproject.data

import java.time.DayOfWeek


data class TeacherTime(val name: String = "", val pic: String = "", val startEndDay: String = "", val teachTime: MutableList<TeachTime> = mutableListOf())

data class TeachTime(val week: DayOfWeek, val dayNum: String = "", val time: MutableList<Time> = mutableListOf())

data class Time(val time: String = "", val isUseTime: Boolean = false)