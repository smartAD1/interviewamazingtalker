package com.example.interviewproject.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewproject.data.TeachTime
import com.example.interviewproject.data.Time

class TimeAdapter : ListAdapter<Time, RecyclerView.ViewHolder>(DiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        TODO("adapter部分新增用addView")
    }

}


class DiffUtil() : DiffUtil.ItemCallback<Time>() {
    override fun areItemsTheSame(oldItem: Time, newItem: Time) = oldItem.time == newItem.time

    override fun areContentsTheSame(oldItem: Time, newItem: Time) = oldItem.isUseTime != newItem.isUseTime

    override fun getChangePayload(oldItem: Time, newItem: Time): Any? {
        return super.getChangePayload(oldItem, newItem)
    }
    //    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
//        return oldItem == newItem
//    }
//
//    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
//        return oldItem == newItem
//    }
}