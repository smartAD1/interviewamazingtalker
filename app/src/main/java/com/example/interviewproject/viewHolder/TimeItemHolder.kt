package com.example.interviewproject.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.example.interviewproject.data.TeachTime
import com.example.interviewproject.data.Time
import com.example.interviewproject.databinding.CustomMeetTimeViewBinding
import com.example.interviewproject.databinding.ItemDayBinding
import com.example.interviewproject.databinding.ItemMeetTimeBinding

class TimeItemHolder(val itemDayBinding: ItemDayBinding): RecyclerView.ViewHolder(itemDayBinding.root) {
    val day = itemDayBinding.week
    val dayNum = itemDayBinding.dayNum

    fun bind(teachTime: TeachTime) {
        day.text = teachTime.startEndDay
        dayNum.text = teachTime.dayNum
        teachTime.time.forEach {
            val itemMeetTimeBinding = ItemMeetTimeBinding.bind(itemDayBinding.root)
            itemMeetTimeBinding.tvTimeItem.text = it.time
        }
    }
}