package com.example.interviewproject

import com.example.interviewproject.data.TeacherTime
import java.util.*

sealed class Status<out T> {
    data class Success(val teacherTime: TeacherTime): Status<TeacherTime>()
    data class Error(val throwable: Throwable): Status<Nothing>()
    object Empty: Status<Nothing>()
    object Loading: Status<Nothing>()
}