package com.example.interviewproject

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.example.interviewproject.adapter.TimeAdapter
import com.example.interviewproject.data.TeachTime
import com.example.interviewproject.data.TeacherTime
import com.example.interviewproject.data.Time
import com.example.interviewproject.databinding.CustomMeetTimeViewBinding

class CustomTimeView(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val timeView = CustomMeetTimeViewBinding.inflate(LayoutInflater.from(context))
    private var timeAdapter = TimeAdapter()
    private var page = 0


    private fun setData(list: MutableList<Time>) {
        timeView.rvTime.adapter = timeAdapter
        timeAdapter.submitList(list)
    }

    fun dataStatus(status: Status<TeacherTime>) {
        when (status) {
            is Status.Success -> {
                timeView.tvTitle.text = status.teacherTime.startEndDay
                setData(status.teacherTime.teachTime[page].time)
            }
            is Status.Error -> status.throwable
            is Status.Empty -> timeView.run {
                rvTime.isGone = true
                tvEmpty.isVisible = true
            }
            is Status.Loading -> {

            }
        }
    }


}